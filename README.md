# PORTAL #

This project is an attempt to create a simple, well designed and expandable framework for website of any size. It ought to be feasible to create a simple website with static content as well as a growing online shop with dozens of sub modules.

### What is this repository for? ###

This repository hosts the source code for the entire project.
Version 0.1

## Basic Idea ##

This project was created to provide a simple yet scalable skeleton website in PHP. Strongly leaned on the MVC design pattern the system is split into several abstract units that try to divide control and data from layout.

* **Controller**
These classes are responsible to calculate and organise the subordinated elements of a layout by using the input and the abstract data layer. In the example website these classes represent page each. However, they are not bound to be web pages nor do they have to have a visible output.

* **Views**
These classes create the receivable response to a request. Not every request must generate a response and not every response must be in HTML. With this abstraction it is possible to create View classes for API layers or other forms of exchange. In the example code you see that HTML views usually have a additional template file which can be altered via the controlling view class.

* **Modules**
Modules are a convenient way to create any sort of view out of basic elements. The idea is that modules are basically the same no matter where they are used on the website can be combined as needed. Also, modules can be build from other modules which makes complex structures possible that can be easily maintained. Modules like Views usually have templates that are filled when used.

* **Models**
These classes are abstract interfaces for common used data, independent from the database or data access that manages it.

* **Application**
This class manages the general function of the application. It sanitises and normalises the input. It includes the necessary resources, checks permission and invokes the Controller. The example shows that all requests are redirected to a main entry point and the URI path is used in a REST-like fashion http://<host>/<controller>/<action>/<parameter1>/<parameter2>...

### Setup ###

This is a PHP application an can be copied/cloned into a directory of your PHP enabled web server.
To add a new web page a new Controller must be added. 

### Configuration ###
In Settings/general.php live the application settings which can easily be adopted to your liking.

### Dependencies ###
This project tries to reduce dependencies but still be able to be used with up to date 3rd party software. The example shows the use of SCSS which can be dropped if necessary. The project is also prepared to use phpunit for testing.

### Database configuration ###
There is no Database necessary but the example contains a abstract Database class that can be implemented for different Databases.

### How to run tests ###
Follow the php unit guidelines.

### Deployment instructions ###
After you created your web application it should can be packed and deployed to any other web server. Please take a moment and make sure you are compliant with the MIT license.