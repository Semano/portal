<?

/**
 * This file contains all global Settings which are necessary for the application
 * @author Johannes Holtz
 * @version 0.1 initial
 */
/* * * error reporting on ** */
error_reporting(E_ALL);
/* * * DEBUG SWITCH ** */
define('DEBUG', '1');

session_save_path("./tmp/");
/* * * public asset directory * * */
define('DIR_PUBLIC', '/public/');

/**
 * Returns a list of files in a given directory. Collecting all children recursivley.
 */
function _ListIn($dir, $prefix = '') {
     $dir = rtrim($dir, '\\/');
     $result = array();

     /* go throuh all files in directory */
     foreach (scandir($dir) as $f) {
          /* if file does not start with a '.' */
          if (preg_match('/^\..*$/', $f) === 0) {
               /* if it is a directory add results from directory */
               if (is_dir($dir . DIRECTORY_SEPARATOR . $f)) {
                    $result = array_merge($result, _ListIn($dir . DIRECTORY_SEPARATOR . $f, $prefix . $f . DIRECTORY_SEPARATOR));
               } else {
                    $result[] = $prefix . $f;
               }
          }
     }
     return $result;
}

function _autoloadClass($class) {
     $_ALL_CLASSES  = _ListIn(__SITE_PATH);
     $filename      = $class . '.class.php';
     $filename      = addcslashes($filename, ".");
     $regex         = "/(^|\\" . DIRECTORY_SEPARATOR . ")" . $filename . "$/i";
     foreach ($_ALL_CLASSES as $dir) {
          if (preg_match($regex, $dir) === 1) {
               include_once ($dir);
               return;
          }
     }
     if (DEBUG) {
          error_log("could not find $class");
     }
}

/**
 * Register autolod function which loads every class automatically if it is needed during a page run
 */
spl_autoload_register("_autoloadClass");
