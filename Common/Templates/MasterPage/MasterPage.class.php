<?

/**
 * This is the main home page class.
 */
class MasterPage extends HTMLView {

     public $header;
     public $footer;
     public $left;
     public $right;
     public $middle;

     public function __construct() {
          parent::__construct(NULL);
          array_push($this->styles, "basic.css.css");
          $this->header = (new Header( ))->render();
          $this->footer = (new Footer( ))->render();
          $this->left = "left";
          $this->right = "right";
          $this->middle = "middle";
     }

     public function assemble() {
          ob_start();
          include "MasterPage.tmpl.php";
          $this->body = ob_get_clean();
     }
}
