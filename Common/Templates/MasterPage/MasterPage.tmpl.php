
<?= $this->header; ?>
<div id="mainContent" class="parallelBox">
     <div id="leftColumn" class="parallelItem" style="width:15%">
          <?= $this->left; ?>
     </div>
     <span>&nbsp;</span>
     <div id="middleColumn" class="parallelItem center_box" style="width:15%;">
          <?= $this->middle; ?>
     </div>
     <span>&nbsp;</span>
     <div id="leftColumn" class="parallelItem" style="width:15%">
          <?= $this->right; ?>
     </div>
     <span class="stretch"></span>
</div>
<?= $this->footer; ?>

