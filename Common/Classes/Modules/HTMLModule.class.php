<?

/*
 * This is a base class for a reusable HTMLmodule that renders a snippet of HTML which in turn can 
 * be plucked into different HTML views
 *
 * @author holtz
 */

abstract class HTMLModule extends Module {

     public $styles = array();
     public $scripts = array();
     public $thirdPartyScripts = array();

}
