<?

/**
 * Views can contains Modules which are content blocks that can be reused.
 */
abstract class Module {

    protected $content;

    public abstract function render();
}
