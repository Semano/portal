<?

/**
 * This is the base class for every controller
 */
abstract class Controller {

     /** @var Databse * */
     protected $database;

     function __construct(Database $db = NULL) {
          $this->database = $db;
     }

     public function accessGranted() {
          /* dummy acceptance */
          return true;
     }

     /* standard action for each Controller */

     public abstract function index();
}
