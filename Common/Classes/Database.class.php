<?

/**
 * This class encapsulates the database connection and accesses.
 */
abstract class Database {

     protected $db;

     public abstract function openConnection();

     public abstract function select($query);

     public abstract function insertRow($table, $row);

     public abstract function insertOrUpdateRow($table, $row);
}
