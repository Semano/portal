<?

/**
 * This is the abstract base class for every externally receivable unit. 
 * That includes web pages as well as other forms of output like APIs or XML interfaces.
 * Sub classes are for example web views 
 */
abstract class View {

     protected $title = "base View";
     protected $loged = FALSE;
     protected $public = TRUE;
     protected $internal = FALSE;
     protected $enabled = TRUE;
     protected $body = '';

     public function __construct($args) {
          if ($this->enabled) {
               if ($this->internal) {
                    //do some preperation
               }
               if ($this->loged) {
                    syslog(LOG_INFO, "View: " . get_class($this) . " was called");
               }
               if (!$this->public) {
                    if (!isAuthorized($args)) {
                         syslog(LOG_AUTHPRIV, "Not Authorized");
                         // drop Request;
                         throw new Exception("Not Authorized");
                    }
               }
          }
     }
     
     public abstract function render();
}
