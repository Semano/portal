<?

/**
 * This class creates the a JSON API response.
 *
 * @author holtz
 */
abstract class JSONView extends View {

     public final function render() {
          header('Content-Type: text/json; charset=UTF-8');
          ob_start();
          echo $this->body;
          return ob_get_clean();
     }

}
