<?

/**
 * Base class for every HTML page. It contains handlers to add Styles and Scripts. It also 
 * implements the render() function that combines all assets. It creates a basic HTML frame and 
 * includes the given styles and scripts together with the body.
 *
 * @author JohannesHoltz
 */
abstract class HTMLView extends View {

    protected $metas = array();
    protected $styles = array();
    protected $scripts = array();
    protected $externalScripts = array();
    protected $HTMLExtScripts = '';
    protected $HTMLStyles = '';
    protected $HTMLScripts = '';

    public function __construct($args) {
        // call parent constructor first to get all it's features
        parent::__construct($args);
    }

    public function addScript($script) {
        $this->scripts[] = $script;
    }

    public function addExtScript($script) {
        $this->externalScripts[] = $script;
    }

    public function addStyle($style) {
        $this->styles[] = $style;
    }

    private function combineAssets() {
        foreach ($this->externalScripts as $srcURL) {
            $this->HTMLExtScripts .= "<script src=\"$srcURL\"></script>\n";
        }
        foreach ($this->styles as $styleURL) {
            $this->HTMLStyles .= '<link rel="stylesheet" media="all" type="text/css" href="' . DIR_PUBLIC . 'CSS/' . $styleURL . '"/>';
        }
        foreach ($this->scripts as $script) {
            $this->HTMLScripts .= $script . '\n';
        }
    }

    abstract public function assemble();
    
    /*
     * This function builds the actual HTML response in form of a string. It should use the 
     * embedded class members to do so.
     */
    public final function render() {
        $this->combineAssets();
        header('Content-Type: text/html; charset=UTF-8');
        ob_start();
        require_once( 'HTMLView.tmpl.php');
        return ob_get_clean();
    }

}
