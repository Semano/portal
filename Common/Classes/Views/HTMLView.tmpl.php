<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $this->title ?></title>
        <?= $this->HTMLExtScripts ?>
        <?= $this->HTMLStyles ?>
        <?= $this->HTMLScripts ?>
    </head>
    <body>
        <?= $this->body ?>
    </body>
</html>
