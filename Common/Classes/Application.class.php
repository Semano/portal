<?

/**
 * This class is the base application which calls the right controller and
 * handles the requests
 */
class Application {

     private static $controller;
     private static $action;
     private static $arguments = array();

     public static function run($request) {
          self::startSession($request);

          $parts = self::splitUrl($request);
          self::$controller = $parts['controller'];
          self::$action = $parts['action'];
          self::$arguments = $parts['arguments'];

          /* if controller for the requested site exist then load this file and create this controller */
          if (isset(self::$controller) && file_exists('./Controller/' . self::$controller . '.class.php')) {

               /* use controller name and overwrite with class instance */
               self::$controller = new self::$controller( );

               /* check for method: does such a method exist in the controller ? */
               if (method_exists(self::$controller, self::$action)) {
                    if (self::$controller->accessGranted()) {
                         self::$controller->{self::$action}(explode(',', self::$arguments));
                    }
               } else {
                    /* default/fallback: call the index() method of a selected controller */
                    self::$controller->index();
               }
          } else {
               /* invalid URL, so simply show home/index */
               (new Home())->index();
          }
     }

     private static function splitUrl($req) {
          $arguments = array();
          $controller = null;
          $action = null;
          if (isset($req)) {

               /* split URL REST like */

               /* remove trailing '/' */
               $url = rtrim($req, '/');
               /* remove all disallowed characters */
               $url = filter_var($url, FILTER_SANITIZE_URL);
               /* split into different parts on '/' */
               $url = explode('/', $url);

               /*
                * Put URL parts into according properties 
                * Only one dedicated controller 
                * Only one dedicated action
                * Multiple arguments
                */
               $controller = (isset($url[1]) ? $url[1] : null);
               $action = (isset($url[2]) ? $url[2] : null);

               for ($i = 3; $i < count($url); $i++) {
                    if (isset($url[$i])) {
                         array_push($arguments, $url[$i]);
                    }
               }
          }
          return array(
              "controller" => $controller,
              "action" => $action,
              "arguments" => $arguments
          );
     }

     private static function startSession($req) {
          if (!session_start()) {
               error_log("Unable to create Session for requests " + $req);
          }
     }

}
