<?

/**
 * A stadard Login Box
 *
 * @author holtz
 */
class LoginBox extends HTMLModule {

     public function render() {
          ob_start();
          require 'LoginBox.tmpl.php';
          return ob_get_clean();
     }

}
