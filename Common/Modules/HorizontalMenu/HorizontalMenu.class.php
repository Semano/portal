<?

/**
 * Horizontal page menu
 *
 * @author holtz
 */
class HorizontalMenu extends HTMLModule{
     
     private $menuEntries = array(
         'Home'     =>   'Home',
         'Login'    =>   'Login',
         'About'    =>   'About'
     );
     
     public function render() {
          ob_start();
          include 'HorizontalMenu.tmpl.php';
          return ob_get_clean();
     }
}
