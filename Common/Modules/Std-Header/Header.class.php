<?

/**
 * Standard Header
 */
class Header extends HTMLModule {

     private $logoSrc = "https://cdn.tutsplus.com/net/uploads/legacy/2013_phpvsmysqli/tutorial_1.png";
     private $hMenu = '';
     
     public function render() {
          $this->hMenu = (new HorizontalMenu())->render();
          ob_start();
          include 'Header.tmpl.php';
          return ob_get_clean();
     }

}
