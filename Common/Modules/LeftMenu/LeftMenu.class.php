<?
class LeftMenu extends HTMLModule {
     
     private $menuEntries = array( 
         'Home'     =>   'Home',
         'About'    =>   'About',
         
     );
     
     public function render() {
          ob_start();
          require 'LeftMenu.tmpl.php';
          return ob_get_clean();
     }
     
     public function addMenuEntry( $entry ) {
          $this->menuEntries[] = $entry;
     }
     
     public function setMenu( $menu ) {
          $this->menuEntries = $menu;
     }
}
