<?

/**
 * Standard Footer
 */
class Footer extends HTMLModule {

    public function render() {
        ob_start();
        include "Footer.tmpl.php";
        return ob_get_clean();
    }

}
