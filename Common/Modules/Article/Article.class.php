<?

class Article extends HTMLModule {

     private $title;
     private $image;
     private $text;

     public function render() {
          ob_start();
          include "Article.tmpl.php";
          return ob_get_clean();
     }
     
     public function setTitle( $title ) {
          $this->title = $title;
     }
     
     public function setImage( $image ) {
          $this->image = $image;
     }
     
     public function setText( $text ) {
          $this->text = $text;
     }

}
