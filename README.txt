/***********************************************/
/* PORTAL - PHP based modular website skeleton */
/***********************************************/

The goal of the Portal project is to develop a light weight modular and highly customizable website skeleton.
Furthermore it implements different features for more complex web applications. For example does it implement a rudimentary authentication system which can be easily extended or replaced.

