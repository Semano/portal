<?
/**
 * MAIN ENTRY POINT for visible web site.
 * This is the main entry point of the application. All request will be directed
 * to this file.
 * @author	Johannes Holtz
 * @version	0.1	this is the first edition
 */
//phpinfo();

/* * * load site constants and settings** */
//*************** PATHS *****************//
define('__SITE_PATH', realpath( dirname(__FILE__) ) );

function _main() 
{
    require_once ('Settings' . DIRECTORY_SEPARATOR . 'general.php');

    $url = $_SERVER['REQUEST_URI'];
    //FIXME: not yet implemented -> filter_input( INPUT_REQUEST, $url, FILTER_SANITIZE_URL );
    Application::run($url);
    
}
_main();
