<?

/**
 * Generic MySQL/PDO abstraction layer for database access
 */
class MySQL extends Database {

     private $selected;

     function __construct() {
          $this->openConnection();
     }

     function openConnection() {
          $this->db = new PDO("mysql:host=" . DatabaseSettings::mainIP . ":" . DatabaseSettings::Port 
                              . ";dbname=" . DatabaseSettings::Name, 
                              DatabaseSettings::User, 
                              DatabaseSettings::Password);
          if (isset($this->db)) {
               if ($this !== NULL) {
                    return true;
               }
          }
          return false;
     }

     public function select($query) {
          $this->db->query($query);
     }

     public function insertRow($table, $row) {
          throw new Exception("Not yet implemented");
     }

     public function insertOrUpdateRow($table, $row) {
          throw new Exception("Not yet implemented");
     }

}
