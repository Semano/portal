<?

/*
 * Login page to access restricted area
 *
 * @author holtz
 */

class Login extends Controller {

     /** @var HTMLView */
     private $view;

     public function __construct() {
          $db = new MySQL();
          $this->view = new MasterPage();

          parent::__construct($db);
     }

     public function index() {
          assert(is_a($this->view, "HTMLView"), "View " . $this->view . "is not a valid HTMLView");
          $this->view->left = new LeftMenu();
          $this->view->middle = new LoginBox();
     }

}
