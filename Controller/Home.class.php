<?

/**
 * This is the default page if no controller was detected
 */
class Home extends Controller {

     private $page;

     function __construct() {
          $db = NULL;//new MySQL( );
          parent::__construct($db);
          
          $this->page = new MasterPage();
          $menu = new LeftMenu();
          /* FIXME: create a better solution for the page index */
          $menu->setMenu(array('Home','About'));
          $this->page->left = $menu->render();
     }
     
     /* standard action is to render the page */
     public function index() {
          $this->page->assemble();
          echo $this->page->render();
     }

}
