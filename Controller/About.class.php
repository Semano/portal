<?

/**
 * About page with information about the page and the author
 *
 * @author holtz
 */
class About extends Controller {

     private $view;

     public function __construct() {
          $db = NULL;
          $this->view = new MasterPage();
          parent::__construct($db);
     }

     public function index( ) {
          $this->html();
     }
     
     public function html( ) {
          
          $this->view->header = (new Header())->render();
          $article = new Article();
          $article->setTitle("About This Site");
          $article->setText("This is a pseudo article. lore ipsum kjhfalsdkfjab  u 3l281 ,.amc [092 ;elore ipsum kjhfalsdkfjab  u 3l281 ,.amc [092 ;elore i");
          
          $menu = new LeftMenu();
          $menu->setMenu(array('Home','About'));
          
          $this->view->left = $menu->render();
          $this->view->middle = $article->render();
          
          echo $this->view->render();
     }
     
     public function simple( ) {
          $this->view = new SimplePage();
          echo $this->view->render();
     }

}
